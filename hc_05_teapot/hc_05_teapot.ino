// Include the libraries we need
#include <OneWire.h>
#include <DallasTemperature.h>
#include <EEPROM.h>

#define ONE_WIRE_PIN 2
#define RELAY_PIN 	 4
#define BUZZER_PIN   3
#define LED_PIN   	 13

#define EEPROM_RELAY_STATE_ON  0x55
#define EEPROM_RELAY_STATE_OFF 0xAA


OneWire oneWire(ONE_WIRE_PIN);
DallasTemperature sensors(&oneWire);
byte eeprom_val = eeprom_val;


void setup(void)
{
  // start serial port
  Serial.begin(9600);
  Serial.println("start");

  // Start up the library
  sensors.begin();

  pinMode(RELAY_PIN, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);

  eeprom_val = EEPROM.read(0);
  if (EEPROM_RELAY_STATE_ON == eeprom_val) {

  } else if (EEPROM_RELAY_STATE_ON == eeprom_val) {

  } else {

  }
}

/*
 * Main function, get and show the temperature
 */
void loop(void)
{ 

  //value = EEPROM.read(address);
  //EEPROM.write(addr, val);
  
  // call sensors.requestTemperatures() to issue a global temperature 
  // request to all devices on the bus
  //Serial.print("Requesting...");
  //sensors.requestTemperatures(); // Send the command to get temperatures
  //Serial.println("DONE");
  // After we got the temperatures, we can print them here.
  // We use the function ByIndex, and as an example get the temperature from the first sensor only.
  //Serial.println(sensors.getTempCByIndex(0));  

}
